# Software Studio 2018 Spring Assignment 02 小朋友下樓梯

## 小朋友下樓梯
<img src="example01.png" width="700px" height="500px"></img>

## Goal
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Complete a game "小朋友下樓梯" by Phaser. (JavaScript or TypeScript)
3. Your game should reach the basic requirements.
4. You can download needed materials from some open source webpage to beautify the appearance.
5. Commit to "your" project repository and deploy to Gitlab page.
6. **Report which items you have done and describing other functions or feature in REABME.md.**

## Scoring 
|                                              Item                                              | Score | Y or N |
|:----------------------------------------------------------------------------------------------:|:-----:|:------:|
| A complete game process: start menu => game view => game over => quit or play again            |  20%  |   Y   |
| Your game should follow the basic rules of  "小朋友下樓梯".                                    |  15%  |   Y   |
|         All things in your game should have correct physical properties and behaviors.         |  15%  |   Y   | 
| Set up some interesting traps or special mechanisms. .(at least 2 different kinds of platform) |  10%  |   Y   |
| Add some additional sound effects and UI to enrich your game.                                  |  10%  |   Y   |
| Store player's name and score in firebase real-time database, and add a leaderboard to your game.        |  10%  |   Y   |
| Appearance (subjective)                                                                        |  10%  |   Y   |
| Other creative features in your game (describe on README.md)                                   |  10%  |   Y   |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/Assignment_02**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, .ts, etc.
    * source files
* **Deadline: 2018/05/24 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

## Report
* State:
    此次作業中我設了6個State，分別是boot、load、menu、play、duo和over:
1. boot: 載入在load state時所要用到的進度條、載入排行榜與啟動物理引擎，再跳到load。
2. load: 載入遊戲中所需的素材(音樂、圖片等)，再跳到menu state。
3. menu: 顯示主畫面並播放背景音樂，此時按下enter會要求輸入玩家名稱並跳到單人模式(play state);
         按下spacebar會要求輸入隊伍名稱並跳到雙人模式(duo state)。
4. play: 撥放音樂並進行單人遊戲的state，共有6種平台，一般、針刺、彈簧、翻轉、滾動(左右)，被針刺到會有音效。
5. duo: 雙人遊戲進行的state，基本上都與單人一樣，可兩人同時遊玩。
6. over: 當player的生命到0或掉落置邊界外時遊戲結束，進入over state。

* Other Function:
1. 雙人模式: 除了原本的player外新增另一名player，以競爭、合作的方式增加遊戲樂趣。
2. 平台加速: 當達到一定的樓層後，平台往上的速度會變快。