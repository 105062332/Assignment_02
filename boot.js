var bootState = {
    preload: function () { 
        // Load the progress bar image. 
        game.load.image('progressBar', 'assets/progressBar.png'); 
    }, 
    create: function() { // Set some game settings. 
        //game.stage.backgroundColor = '#3498db';
        var leaderboard = firebase.database().ref('leaderboard').orderByChild('score');
        var board = document.getElementById('leaderboard');
        var total_post = [];
        var actotal_post = [];

        leaderboard.once('value')
        .then(function(snapshot){
            snapshot.forEach(function(childSnapshot){
                if(childSnapshot.score!==null)
                {
                    var childData = childSnapshot.val();
                    total_post[total_post.length] = '. '+childData.name+' B'+childData.score+'</h5></div></pre>';
                }
            })
            if(total_post.length>=10)
            {
                for(var i = total_post.length; i>total_post.length-10 ;i--)
                {
                    actotal_post[actotal_post.length] = '<pre><div><h5>  '+(actotal_post.length+1)+total_post[i-1];
                }
            }
            else
            {
                for(var i = total_post.length; i>0; i--)
                {
                    actotal_post[actotal_post.length] = '<pre><div><h5>  '+(actotal_post.length+1)+total_post[i-1];
                }
            }
            board.innerHTML = actotal_post.join('');
        });

        var leaderboard2 = firebase.database().ref('duo').orderByChild('score');
        var board2 = document.getElementById('leaderboard2');
        var total_post2 = [];
        var actotal_post2 = [];
        leaderboard2.once('value')
        .then(function(snapshot){
            snapshot.forEach(function(childSnapshot){
                if(childSnapshot.score!==null)
                {
                    var childData = childSnapshot.val();
                    total_post2[total_post2.length] = '. '+childData.name+' B'+childData.score+'</h5></div></pre>';
                }
            })
            if(total_post2.length>=10)
            {
                for(var i = total_post2.length; i>total_post2.length-10 ;i--)
                {
                    actotal_post2[actotal_post2.length] = '<pre><div><h5>  '+(actotal_post2.length+1)+total_post2[i-1];
                }
            }
            else
            {
                for(var i = total_post2.length; i>0; i--)
                {
                    actotal_post2[actotal_post2.length] = '<pre><div><h5>  '+(actotal_post2.length+1)+total_post2[i-1];
                }
            }
            board2.innerHTML = actotal_post2.join('');
        });
        
        game.physics.startSystem(Phaser.Physics.ARCADE);
        //game.renderer.renderSession.roundPixels = true; 
        // Start the load state. 
        game.state.start('load'); 
    }

};