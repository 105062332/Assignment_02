var lastTime = 0;
var platforms = [];
var distance = 0;
var once = 0;

var duoState = {
    preload: function(){
        /*game.load.spritesheet('player', 'assets/player.png', 32, 32);
        game.load.image('wall', 'assets/wall.png');
        game.load.image('ceiling', 'assets/ceiling.png');
        game.load.image('normal', 'assets/normal.png');
        game.load.image('nails','assets/nails.png');
        game.load.spritesheet('conveyorLeft', 'assets/conveyor_left.png', 96, 16);
        game.load.spritesheet('conveyorRight', 'assets/conveyor_right.png', 96, 16);
        game.load.spritesheet('trampoline', 'assets/trampoline.png', 96,22);
        game.load.spritesheet('fake', 'assets/fake.png', 96, 36);*/
    },
    create: function(){

        game.global.state = 2;
        this.hurt = game.add.audio('nail');
        this.back_music = game.add.audio('gameback',0.7, true);
        this.back_music.play();
        // the walls
        this.walls = game.add.group();
        this.walls.enableBody = true;
        game.physics.arcade.enable(this.walls);
        game.add.sprite(0,0,'wall', 0, this.walls);
        game.add.sprite(783,0,'wall',0, this.walls);
        game.add.sprite(0,400,'wall',0, this.walls);
        game.add.sprite(783,400,'wall',0, this.walls);
        this.walls.setAll('body.immovable', true);

        // the ceilings
        game.add.image(0, 0, 'ceiling');
        game.add.image(400,0,'ceiling');

        //2p
        this.player2 = game.add.sprite(300, 100,'player2');
        game.physics.arcade.enable(this.player2);
        this.player2.body.gravity.y = 500;
        this.player2.animations.add('left', [0,1,2,3],8);
        this.player2.animations.add('right', [9,10,11,12],8);
        this.player2.animations.add('flyleft', [18,19,20,21],12);
        this.player2.animations.add('flyright', [27,28,29,30],12);
        this.player2.animations.add('fly', [36,37,38,39], 12);
        this.player2.animations.add('hurt_left',[4,5,6,7],8);
        this.player2.animations.add('hurt_right',[13,14,15,16],8);
        this.player2.animations.add('hurt_flyleft', [22,23,24,25],12);
        this.player2.animations.add('hurt_flyright',[31,32,33,34],12);
        this.player2.animations.add('hurt_fly',[40,41,42,43],12);
        this.player2.animations.add('hurt',[8,17],8);
        this.player2.life = 10;
        this.player2.touchOn = null;
        this.player2.unbeatableTime = 0;

        this.aKey = game.input.keyboard.addKey(Phaser.Keyboard.A);

        this.dKey = game.input.keyboard.addKey(Phaser.Keyboard.D);

        // players
        this.player = game.add.sprite(500, 100,'player');
        game.physics.arcade.enable(this.player);
        this.player.body.gravity.y = 500;
        this.player.animations.add('left', [0,1,2,3],8);
        this.player.animations.add('right', [9,10,11,12],8);
        this.player.animations.add('flyleft', [18,19,20,21],12);
        this.player.animations.add('flyright', [27,28,29,30],12);
        this.player.animations.add('fly', [36,37,38,39], 12);
        this.player.animations.add('hurt_left',[4,5,6,7],8);
        this.player.animations.add('hurt_right',[13,14,15,16],8);
        this.player.animations.add('hurt_flyleft', [22,23,24,25],12);
        this.player.animations.add('hurt_flyright',[31,32,33,34],12);
        this.player.animations.add('hurt_fly',[40,41,42,43],12);
        this.player.animations.add('hurt',[8,17],8);
        this.player.life = 10;
        this.player.touchOn = null;
        this.player.unbeatableTime = 0;

        // create arrow keys
        this.cursor = game.input.keyboard.createCursorKeys();

        this.createText();
    },
    update: function(){

        game.physics.arcade.collide(this.player, platforms, this.effect, null, this);
        game.physics.arcade.collide(this.player, this.walls);
        game.physics.arcade.collide(this.player2, platforms, this.effect, null, this);
        game.physics.arcade.collide(this.player2, this.walls);
        game.physics.arcade.collide(this.player, this.player2, this.flyaway,null, this);
        this.movePlayer();
        this.movePlayer2();
        this.createPlatform();
        this.updatePlatforms();
        this.checkUpbound(this.player);
        this.checkUpbound(this.player2);
        this.updateText();
        this.checkLife();

    },

    flyaway: function(){
        this.player.velocity *= -2;
        this.player2.velocity *= -2; 
    },

    checkLife: function(){
        if((this.player.life<=0 || this.player.body.y>600) && (this.player2.life<=0 || this.player2.body.y>600))
        {
            //platforms.forEach(function(s) {s.destroy()});
            platforms = [];
            var postData = {
                name: game.global.name,
                score: distance
            };
            distance = 0;
            firebase.database().ref('duo').push(postData);
            this.back_music.stop(0);

            game.state.start('over');
        }
        if(this.player.life<=0 || this.player.body.y>600)
        {
            this.player.visible = false;
            this.player.life = 0;
            this.player.kill();
        }
        if(this.player2.life<=0 || this.player2.body.y>600)
        {
            this.player2.visible = false;
            this.player2.life = 0;
            this.player2.kill();
        }
    },

    updateText: function(){
        this.text1.setText('P1-life: '+ this.player.life);
        this.text3.setText('P2-life: '+ this.player2.life);
        this.text2.setText('B '+ distance);
    },

    checkUpbound: function(player){
        if(player.body.y<0)
        {
            player.body.y = 0;
            if(player.body.velocity.y<0)
            {
                player.body.velocity.y = 0;
            }
            if(game.time.now > player.unbeatableTime) {
                this.hurt.play();
                player.life -= 3;
                game.camera.flash(0xff0000, 100);
                game.camera.shake(0.02, 300);
                player.unbeatableTime = game.time.now + 2000;
            }
        }
    },

    effect: function(player, platform){
        if(platform.key == 'conveyorRight') {
            this.conveyorRightEffect(player, platform);
        }
        if(platform.key == 'conveyorLeft') {
            this.conveyorLeftEffect(player, platform);
        }
        if(platform.key == 'trampoline') {
            this.trampolineEffect(player, platform);
        }
        if(platform.key == 'nails') {
            this.nailsEffect(player, platform);
        }
        if(platform.key == 'normal') {
            this.basicEffect(player, platform);
        }
        if(platform.key == 'fake') {
            this.fakeEffect(player, platform);
        }
    },

    playAnima: function(){
        var x = this.player.body.velocity.x;
        var y = this.player.body.velocity.y;

        if(x<0 && y>0)
        {
            if(this.player.unbeatableTime < game.time.now)
                this.player.animations.play('flyleft');
            else
                this.player.animations.play('hurt_flyleft');
        }
        if(x>0 && y>0)
        {
            if(this.player.unbeatableTime < game.time.now)
                this.player.animations.play('flyright');
            else
                this.player.animations.play('hurt_flyright');
        }
        if(x<0 && y==0)
        {
            if(this.player.unbeatableTime < game.time.now)
                this.player.animations.play('left');
            else
                this.player.animations.play('hurt_left');
        }
        if(x>0 && y==0)
        {
            if(this.player.unbeatableTime < game.time.now)
                this.player.animations.play('right');
            else
                this.player.animations.play('hurt_right');
        }
        if(x==0 && y!=0)
        {
            if(this.player.unbeatableTime < game.time.now)
                this.player.animations.play('fly');
            else
                this.player.animations.play('hurt_fly');
        }
        if(x==0 && y==0)
        {
            if(this.player.unbeatableTime < game.time.now)
                this.player.frame = 8;
            else
                this.player.animations.play('hurt');
        }
    },

    playAnima2: function(){
        var x = this.player2.body.velocity.x;
        var y = this.player2.body.velocity.y;

        if(x<0 && y>0)
        {
            if(this.player2.unbeatableTime < game.time.now)
                this.player2.animations.play('flyleft');
            else
                this.player2.animations.play('hurt_flyleft');
        }
        if(x>0 && y>0)
        {
            if(this.player2.unbeatableTime < game.time.now)
                this.player2.animations.play('flyright');
            else
                this.player2.animations.play('hurt_flyright');
        }
        if(x<0 && y==0)
        {
            if(this.player2.unbeatableTime < game.time.now)
                this.player2.animations.play('left');
            else
                this.player2.animations.play('hurt_left');
        }
        if(x>0 && y==0)
        {
            if(this.player2.unbeatableTime < game.time.now)
                this.player2.animations.play('right');
            else
                this.player2.animations.play('hurt_right');
        }
        if(x==0 && y!=0)
        {
            if(this.player2.unbeatableTime < game.time.now)
                this.player2.animations.play('fly');
            else
                this.player2.animations.play('hurt_fly');
        }
        if(x==0 && y==0)
        {
            if(this.player2.unbeatableTime < game.time.now)
                this.player2.frame = 8;
            else
                this.player2.animations.play('hurt');
        }
    },

    movePlayer: function(){
        if(this.cursor.left.isDown)
        {
            this.player.body.velocity.x = -250;
        }
        else if(this.cursor.right.isDown)
        {
            this.player.body.velocity.x = 250;
        }
        else
        {
            this.player.body.velocity.x = 0;
        }
        this.playAnima();
    },

    movePlayer2: function(){
        if(this.aKey.isDown)
        {
            this.player2.body.velocity.x = -250;
        }
        else if(this.dKey.isDown)
        {
            this.player2.body.velocity.x = 250;
        }
        else
        {
            this.player2.body.velocity.x = 0;
        }
        this.playAnima2();
    },

    createPlatform: function(){
        if(game.time.now > lastTime + 300)
        {
            distance += 1;
            lastTime = game.time.now;
            
            var platform;
            var x = Math.random()*(800-96-40)+20;
            var y = 600;
            var rand = Math.random()*100;

            if(rand < 20) {
                platform = game.add.sprite(x, y, 'normal');
            } else if (rand < 40) {
                platform = game.add.sprite(x, y, 'nails');
                game.physics.arcade.enable(platform);
                platform.body.setSize(96, 15, 0, 15);
            } else if (rand < 50) {
                platform = game.add.sprite(x, y, 'conveyorLeft');
                platform.animations.add('scroll1', [0, 1, 2, 3], 16, true);
                platform.play('scroll1');
            } else if (rand < 60) {
                platform = game.add.sprite(x, y, 'conveyorRight');
                platform.animations.add('scroll2', [0, 1, 2, 3], 16, true);
                platform.play('scroll2');
            } else if (rand < 80) {
                platform = game.add.sprite(x, y, 'trampoline');
                platform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
                platform.frame = 3;
            } else {
                platform = game.add.sprite(x, y, 'fake');
                platform.animations.add('turn', [0, 1, 2, 3, 4, 5, 0], 14);
            }
        
            game.physics.arcade.enable(platform);
            platform.body.immovable = true;
            platforms.push(platform);
        
            platform.body.checkCollision.down = false;
            platform.body.checkCollision.left = false;
            platform.body.checkCollision.right = false;
        }
    },
    updatePlatforms: function(){
        for(var i=0; i<platforms.length; i++)
        {
            var platform = platforms[i];
            if(distance>=30)
                platform.body.position.y -= 3;
            else if(distance>=50)
                platform.body.position.y -= 4;
            else
                platform.body.position.y -=2;
            if(platform.body.position.y <= -20) {
                platform.destroy();
                platforms.splice(i, 1);
            }
        }
    },
    conveyorRightEffect: function(player, platform){
        player.body.x+=2;
    },
    conveyorLeftEffect: function(player, platform){
        player.body.x-=2;
    },
    trampolineEffect: function(player, platform){
        platform.animations.play('jump');
        player.body.velocity.y = -350;
    },
    nailsEffect: function(player, platform){
        if(player.touchOn !== platform)
        {
            game.sound.volume = 0.8;
            this.hurt.play();
            player.life -= 3;
            player.touchOn = platform;
            game.camera.flash(0xff0000, 100);
            game.camera.shake(0.02, 300);
        }
    },
    basicEffect: function(player, platform){
        if(player.touchOn !== platform)
        {
            if(player.life<10)
            {
                player.life += 1;
            }
            player.touchOn = platform;
        }
    },
    fakeEffect: function(player, platform){
        if(player.touchOn !== platform) {
            platform.animations.play('turn');
            setTimeout(function() {
                platform.body.checkCollision.up = false;
            }, 100);
            player.touchOn = platform;
        }
    },

    createText: function(){
        var style = {fill: '#ff0000', font: '40px Arial'};

        this.text1 = game.add.text(30, 10, '', style);
        this.text3 = game.add.text(30, 40, '', style);
        this.text2 = game.add.text(680,10, '', style);
    }

}

/*var game = new Phaser.Game(800, 600, Phaser.AUTO, 'canvas');
game.state.add('main', mainState);
game.state.start('main');*/