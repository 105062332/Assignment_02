var game = new Phaser.Game(800, 600, Phaser.AUTO, 'canvas');

game.global = { name, state: 0 };

game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('play', playState);
game.state.add('duo', duoState);
game.state.add('over', overState);

game.state.start('boot');