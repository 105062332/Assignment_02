var loadState = {
    preload: function(){
        var loadingLabel = game.add.text(game.width/2,150, 'loading...', {font: '30px Arial', fill: '#ffffff'});
        loadingLabel.anchor.setTo(0.5, 0.5);

        var progressBar = game.add.sprite(game.width/2, 200, 'progressBar');
        progressBar.anchor.setTo(0.5, 0.5);
        game.load.setPreloadSprite(progressBar);

        game.load.spritesheet('player', 'assets/player.png', 32, 32);
        game.load.spritesheet('player2','assets/player2.png', 32, 32);
        game.load.image('wall', 'assets/wall.png');
        game.load.image('ceiling', 'assets/ceiling.png');
        game.load.image('normal', 'assets/normal.png');
        game.load.image('nails','assets/nails.png');
        game.load.spritesheet('conveyorLeft', 'assets/conveyor_left.png', 96, 16);
        game.load.spritesheet('conveyorRight', 'assets/conveyor_right.png', 96, 16);
        game.load.spritesheet('trampoline', 'assets/trampoline.png', 96,22);
        game.load.spritesheet('fake', 'assets/fake.png', 96, 36);
        game.load.image('background', 'assets/background.png');
        game.load.image('gameover', 'assets/gameover.png');
        game.load.audio('dead', 'assets/dead.mp3');
        game.load.audio('menu', 'assets/menu.mp3');
        game.load.audio('nail', 'assets/nail.mp3');
        game.load.audio('gameback', 'assets/gameback.mp3');
    },
    create: function(){
        game.state.start('menu');
    }
};