var menuState = {
    create:function(){
        game.add.image(0, 0, 'background');

        var nameLabel = game.add.text(game.width/2, 80, 'DownStairs', {font: '80px Arial', fill:'#ffffff'});
        nameLabel.anchor.setTo(0.5, 0.5);

        var startLabel = game.add.text(game.width/2, game.height-160, 'press the spacebar to start 2p', { font: '25px Arial', fill: '#ffffff' });
        startLabel.anchor.setTo(0.5, 0.5);

        var startLabel = game.add.text(game.width/2, game.height-80, 'press the enter key to start 1p', { font: '25px Arial', fill: '#ffffff' });
        startLabel.anchor.setTo(0.5, 0.5);

        var enterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        enterKey.onDown.add(this.start, this);

        var spaceKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        spaceKey.onDown.add(this.start_duo, this);

        this.back = game.add.audio('menu',1,true);
        this.back.play();
    },
    start: function(){
        game.global.name = prompt("Please enter your name");
        this.back.stop(0);
        game.state.start('play');
    },

    start_duo: function(){
        game.global.name = prompt("Please enter the team's name");
        this.back.stop(0);
        game.state.start('duo');
    }
}