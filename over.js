var overState = {
    preload: function(){

    },
    create: function(){
        game.add.image(0, 0, 'gameover');
        game.sound.volume = 0.8;
        this.dead = game.add.audio('dead');
        this.dead.play();
        this.text1 = game.add.text(game.width/2, 80, 'Game Over', {fill: '#ffffff', font: '80px Arial'});
        this.text1.anchor.setTo(0.5, 0.5);
        this.text2 = game.add.text(game.width/2, game.height-160, 'press enter to restart', {fill: '#ffffff', font: '25px Arial'});
        this.text2.anchor.setTo(0.5,0.5);
        this.text3 = game.add.text(game.width/2, game.height-80, 'press esc to quit',{fill: '#ffffff', font: '25px Arial'});
        this.text3.anchor.setTo(0.5,0.5);

        var leaderboard = firebase.database().ref('leaderboard').orderByChild('score');
        var board = document.getElementById('leaderboard');
        var total_post = [];
        var actotal_post = [];
        leaderboard.once('value')
        .then(function(snapshot){
            snapshot.forEach(function(childSnapshot){
                if(childSnapshot.score!==null)
                {
                    var childData = childSnapshot.val();
                    total_post[total_post.length] = '. '+childData.name+' B'+childData.score+'</h5></div></pre>';
                }
            })
            if(total_post.length>=10)
            {
                for(var i = total_post.length; i>total_post.length-10 ;i--)
                {
                    actotal_post[actotal_post.length] = '<pre><div><h5>  '+(actotal_post.length+1)+total_post[i-1];
                }
            }
            else
            {
                for(var i = total_post.length; i>0; i--)
                {
                    actotal_post[actotal_post.length] = '<pre><div><h5>  '+(actotal_post.length+1)+total_post[i-1];
                }
            }
            board.innerHTML = actotal_post.join('');
        });

        var leaderboard2 = firebase.database().ref('duo').orderByChild('score');
        var board2 = document.getElementById('leaderboard2');
        var total_post2 = [];
        var actotal_post2 = [];
        leaderboard2.once('value')
        .then(function(snapshot){
            snapshot.forEach(function(childSnapshot){
                if(childSnapshot.score!==null)
                {
                    var childData = childSnapshot.val();
                    total_post2[total_post2.length] = '. '+childData.name+' B'+childData.score+'</h5></div></pre>';
                }
            })
            if(total_post2.length>=10)
            {
                for(var i = total_post2.length; i>total_post2.length-10 ;i--)
                {
                    actotal_post2[actotal_post2.length] = '<pre><div><h5>  '+(actotal_post2.length+1)+total_post2[i-1];
                }
            }
            else
            {
                for(var i = total_post2.length; i>0; i--)
                {
                    actotal_post2[actotal_post2.length] = '<pre><div><h5>  '+(actotal_post2.length+1)+total_post2[i-1];
                }
            }
            board2.innerHTML = actotal_post2.join('');
        });

        var enterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        enterKey.onDown.add(this.restart, this);

        var escape = game.input.keyboard.addKey(Phaser.Keyboard.ESC);
        escape.onDown.add(this.quit, this);
    },
    restart: function(){
        this.dead.stop(0);
        if(game.global.state == 1)
            game.state.start('play');
        else
            game.state.start('duo');
    },
    quit: function(){
        this.dead.stop(0);
        game.state.start('menu');
    }
};